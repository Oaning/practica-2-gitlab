/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2_gitlab;

import java.util.Scanner;

/**
 *
 * @author DAM108
 */
public class Practica2_GitLab {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner tc = new Scanner(System.in);
        int opc=0;
        
        do{
            
            System.out.println("Introduzca ocpión:");
            System.out.println("\tOpción 1. Cambio de unidades de horas a segundos.");
            System.out.println("\tOpción 2. Cambio de unidades de kilómetros a metros.");
            System.out.println("\tOpción 3. Cambio km/h a m/s.");
            System.out.println("\tOpción 4. Cambio de unidades imperiales al sistema internacional.");
            System.out.println("\tOpción 5. Salir.");
            opc=tc.nextInt();
            switch (opc){
                //Las 4 opciones que tiene el usuario
                case 1:
                    System.out.println("Introduzca hora:");
                    int hora=tc.nextInt();
                    int seg=hora*3600;
                    System.out.println(hora+" horas equivalen a "+seg+" segundos.");
                    break;
                case 2:
                    System.out.println("Introduzca kilómetros:");
                    int km=tc.nextInt();
                    int m=km*1000;
                    System.out.println(km+" kilómetros equivalen a "+m+" metros.");
                    break;
                case 3:
                    System.out.println("Introduzca km/h.");
                    double kmh=tc.nextDouble();
                    double ms=kmh/3.6;
                    System.out.printf("%,.2f km/h equivalen a %,.2f m/s.\n",kmh,ms);
                    break;
                case 4:
                    int opc2=0;
                    do{
                        System.out.println("Introduzca opción:");
                        System.out.println("\t1. Pies a centímetros.");
                        System.out.println("\t2. Pulgadas a milímetros.");
                        System.out.println("\t3. Millas a kilómetros");
                        System.out.println("\t4. Salir.");
                        opc2=tc.nextInt();
                        switch (opc2){
                            case 1:
                                System.out.println("Introduzca pies:");
                                double p=tc.nextDouble();
                                double cm=p*30.48;
                                System.out.println(p+" pies equivalen a "+cm+" centímetros.");
                                break;
                            case 2:
                                System.out.println("Introduzca pulgadas:");
                                double pulgada=tc.nextDouble();
                                double mm=pulgada*25.4;
                                System.out.println(pulgada+" pulgadas equivalen a "+mm+" milímetros.");
                                break;
                            case 3:
                                System.out.println("introduzca millas:");
                                double milla=tc.nextDouble();
                                double kilometro=milla*1.609;
                                System.out.println(milla+" millas equivalen a "+kilometro+" kilómetros.");
                                break;
                            case 4:
                                System.out.println("***Saliendo***");
                                break;
                        }
                        
                    } while (opc2!=4);
                    break;
                case 5:
                    System.out.println("***Saliendo***");
                    break;
            }
        }while (opc!=5);
        
    }
    
}
